#define _GNU_SOURCE
#include <stdio.h>
#include <err.h>
#include <stdlib.h>
#include <readline/readline.h>
#include <readline/history.h>
#include "query.h"
#include "update.h"


/* O que essa função faz é exibir um menu solicitando qual
   dado deve ser atualizado. Depois de recebido o novo valor
   atualizamos a tabela com a consulta apropriada.
   Se houver um único atributo possível de ser atualizado,
   não exibimos o menu.
*/
void update(MYSQL *m,const char * type,const char * key)
{
  /* Mensagens da interação com o usuário usadas frequentemente. */
  const char * oque_atualizar = "O que você deseja atualizar ?\n";
  const char * diga_numero = "Digite o número (Crtl-D para cancelar):";
  const char * opcao_inv = "Opção invalida: %s\n";
  const char * diga_valor = "Digite o novo valor:";
  if (strcmp(type,"property") == 0)
    {
      puts(oque_atualizar);
      printf("(1) Cômodos\n(2)Vagas na Garagem\n(3)Área\n(4)Aluguel\n\n");
      char * l = readline(diga_numero);
      if (l)
	{
	  int t;
	  if (strcmp(l, "1")==0)
	    t = 1;
	  else if (strcmp(l, "2") == 0)
	    t = 2;
	  else if (strcmp(l, "3") == 0)
	    t = 3;
	  else if (strcmp(l, "4") == 0)
	    t = 4;
	  else
	    {
	      printf(opcao_inv,l);
	      return;
	    }

	  free(l);
	  l = readline(diga_valor);
	  char *q;
	  const char * e = "update imóveis set %s = %s where número_certidão = %s";
	  switch (t)
	    {
	    case 1:
	      asprintf(&q, e,"cômodos",l,key);
	      break;
	    case 2:
	      asprintf(&q, e,"vagas_garagem",l,key);
	      break;
	    case 3:
	      asprintf(&q, e,"área",l,key);
	      break;
	    case 4:
	      asprintf(&q, e,"aluguel",l,key);
	      break;
	    }
	  if (mysql_query(m,q) != 0)
	    end_with_error(m);

	  free(q);
	  free(l);
	}
    }
  else if (strcmp(type, "occupant") == 0)
    {
      puts(oque_atualizar);
      puts("(1)Profissão\n(2)Renda familiar\n");

      char * l = readline(diga_numero);
      if (l)
	{
	  char * v = NULL;
	  char * q = NULL;
	  if (strcmp(l, "1") == 0)
	    {
	      v = readline(diga_valor);
	      asprintf(&q, "update inquilino set profissão = \"%s\" where cpf = %s",v,key);
	    }

	  else if (strcmp(l, "2") == 0)
	    {
	      v = readline(diga_valor);
	      asprintf(&q, "update inquilino set renda_familiar = %s where cpf = %s",v,key);
	    }
	  else
	    printf(opcao_inv,l);

	  /* Se houve a alocação da nossa mensagem de query */
	  if (q)
	    {
	      if (mysql_query(m, q) != 0)
		end_with_error(m);

	      free(q);
	    }
	  free(l);
	  if (v)
	    free(v);
	}
    }
  else if (strcmp(type, "owner") == 0)
    {
      char * v = readline("Digite o novo estado civil(Ctrl-D para cancelar):");
      if (v)
	{
	  char * q;
	  asprintf(&q, "update proprietário set estado_civil = \"%s\" where cpf = %s",v,key);

	  if (mysql_query(m, q) != 0)
	    end_with_error(m);

	  free(q);
	  free(v);
	}
    }
  else if (strcmp(type, "guarantor") == 0)
    {
      char * v = readline("Digite o novo saldo bancário(Ctrl-D para cancelar):");
      if (v)
	{
	  char *q;
	  asprintf(&q, "update fiador set saldo_bancário = %s where cpf = %s",v,key);

	  if (mysql_query(m, q) != 0)
	    end_with_error(m);
	  
	  free(q);
	  free(v);
	}
    }
  else if (strcmp(type, "realtor") == 0)
    {
      char * v = readline("Digite a nova comissão(Ctrl-D para cancelar):");
      if (v)
	{
	  char *q;
	  asprintf(&q, "update corretor set comissão = %s where cpf = %s",v,key);

	  if (mysql_query(m, q) != 0)
	    end_with_error(m);
	  
	  free(q);
	  free(v);
	}
    }
  else
    printf("A operação \"%s\" não é conhecida\n",type);
}
