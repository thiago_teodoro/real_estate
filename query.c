#define _GNU_SOURCE
#define QUERY_C
#include <stdio.h>
#include <string.h>
#include <err.h>
#include <stdlib.h>
#include "query.h"

/* Essa enumeração serve para referenciar o vetor abaixo. */
enum
  {
   SHOW_PROPERTIES=0,
   SHOW_PROPERTY,
   SHOW_OFFERS,
   SHOW_CLIENTS_PROPOSAL,
   SHOW_REALTORS_INCOME,
   N_QUERY
  };


/* Aqui temos algumas consultas "especiais". */
const char * query[N_QUERY] = {
			"select número_certidão,aluguel,cpf_proprietário from imóveis order by aluguel DESC",
			"select * from imóveis where número_certidão = ",
			"select cpf_inquilino,data_proposta,valor,data_validade from proposta where número_certidão = ",
			"select cpf,nome from inquilino natural join pessoa where cpf in (select cpf_inquilino from proposta)",

			"select corretor.cpf,count(*)*comissão as rendimento from contrato,corretor "
			"where Year(contrato.data_início) = %s and "
			"exists (select * from visita where "
			"visita.data = contrato.data_início and visita.número_certidão  = número_certidão "
			"and visita.cpf_inquilino = cpf_inquilino and visita.cpf_corretor = corretor.cpf) "
			"group by corretor.cpf order by rendimento DESC;"
};

void end_with_error(MYSQL * m)
{
  /* Imprimimos o erro e encerramos*/
  errx(-1,"%s\n",mysql_error(m));
  if (m)
    mysql_close(m);
  exit(-1);
}

/* Enumeração para referenciar a matriz abaixo. */
enum
  {
   OCCUPANT = 0,
   OWNER,
   GUARANTOR,
   REALTOR,
   PROPERTY,
   PHONE,
   VISIT,
   PROPOSAL,
   CONTRACT,
   PEOPLE,
   N_MAP
  };

/* Matriz que associa o tipo de operação à tabela correspondente
   no banco de dados.
*/
static const char * map[N_MAP][2] =
  {
   {"occupants","inquilino"},
   {"owners","proprietário"},
   {"guarantors","fiador"},
   {"realtors","corretor"},
   {"properties","imóveis"},
   {"phones","telefone"},
   {"visits","visita"},
   {"proposals","proposta"},
   {"contracts","contrato"},
   {"people","pessoa"},
  };


void standard_query(MYSQL * mysql,const char * arg[])
{
  /* Procuramos na matriz qual é a tabela a ser usada. */
  int t = -1;
  for (int i = 0; i < N_MAP; i++)
    if (strcmp(arg[0],map[i][0]) == 0)
      {
	t = i;
	break;
      }

  if (t != -1)
    {
      /* Caso a tabela seja encontrada, gravamos em q 
	 a consulta de seleção adequada.
      */
      char * q;
      if (t == PROPERTY)
	asprintf(&q, query[SHOW_PROPERTIES]);
      else
	asprintf(&q, "select * from %s",map[t][1]);

      if (mysql_query(mysql,q) != 0)
	end_with_error(mysql);

      /* Guardamos o número de linhas a serem exibidas,
	 se aplicável.*/
      int n_rows = (arg[1] == NULL ? -1 : atoi(arg[1]));
      
      MYSQL_RES * result = mysql_store_result(mysql);
      if (result == NULL)
	end_with_error(mysql);


      /* Imprimimos todas as colunas */
      MYSQL_FIELD * field;
      while ((field = mysql_fetch_field(result)))
	printf("%s ",field->name);
      puts("");
      
      /* Finalmente imprimimos todas as linhas. */
      int nf = mysql_num_fields(result);
      MYSQL_ROW  row;
      for (int i = 0; (row = mysql_fetch_row(result)) &&
	     (n_rows > 0 ? i < n_rows : 1);i++)
	{
	  for (int k = 0; k < nf; k++)
	    printf("%s ",row[k]);

	  puts("");
	}
    }
  else
    printf("Comando \"%s\" não reconhecido.\n",arg[0]);
}

/* Similar ao anterior, com a exeção de que a tabela é exibida
   em sua forma transposta.
 */
void property_query(MYSQL * mysql,char * number)
{
  char * str = NULL;

  asprintf(&str,"%s%s",query[SHOW_PROPERTY],number);
  
  
  if (mysql_query(mysql, str) != 0)
    end_with_error(mysql);
      
  MYSQL_RES * result = mysql_store_result(mysql);
  if (result == NULL)
    end_with_error(mysql);

  int nf = mysql_num_fields(result);
  MYSQL_ROW  row;
  while ((row = mysql_fetch_row(result)))
    {
      MYSQL_FIELD * field;

      for (int k = 0; k < nf; k++)
	{
	  field = mysql_fetch_field(result);
	  printf("%s: %s\n",field->name,row[k]);
	}
    }

  free(str);
}

/* Análogo ao standard_query */
void offers_query(MYSQL * mysql,char * number)
{
  char * str = NULL;
  asprintf(&str,"%s%s",query[SHOW_OFFERS],number);
   
  if (mysql_query(mysql, str) != 0)
    end_with_error(mysql);
      
  MYSQL_RES * result = mysql_store_result(mysql);
  if (result == NULL)
    end_with_error(mysql);

  MYSQL_FIELD * field;
  while ((field = mysql_fetch_field(result)))
    printf("%s ",field->name);
  puts("");

  int nf = mysql_num_fields(result);
  MYSQL_ROW  row;
  while ((row = mysql_fetch_row(result)))
    {
      for (int k = 0; k < nf; k++)
	printf("%s ",row[k]);
      puts("");
    }

  free(str);
}

/* Análogo ao standard_query. */
void income_query(MYSQL * mysql,const char * year)
{
  char * q;
  asprintf(&q,query[SHOW_REALTORS_INCOME], year);

  if (mysql_query(mysql, q) != 0)
    end_with_error(mysql);
      
  MYSQL_RES * result = mysql_store_result(mysql);
  if (result == NULL)
    end_with_error(mysql);


  MYSQL_FIELD * field;
  while ((field = mysql_fetch_field(result)))
    printf("%s ",field->name);
  puts("");
      

  int nf = mysql_num_fields(result);
  MYSQL_ROW  row;
  for (int i = 0; (row = mysql_fetch_row(result));i++)
    {
      for (int k = 0; k < nf; k++)
	printf("%s ",row[k]);

      puts("");
    }

  free(q);
}

/* Análogo ao standard query. */
extern void
client_proposal_query(MYSQL *mysql)
{
  if (mysql_query(mysql, query[SHOW_CLIENTS_PROPOSAL]) != 0)
    end_with_error(mysql);
      
  MYSQL_RES * result = mysql_store_result(mysql);
  if (result == NULL)
    end_with_error(mysql);

  MYSQL_FIELD * field;
  while ((field = mysql_fetch_field(result)))
    printf("%s ",field->name);
  puts("");

  int nf = mysql_num_fields(result);
  MYSQL_ROW  row;
  while ((row = mysql_fetch_row(result)))
    {
      for (int k = 0; k < nf; k++)
	printf("%s ",row[k]);
      puts("");
    }
}

extern void
properties_freq_query(MYSQL * m,char * arg[])
{
  char *q;
  asprintf(&q, "select count(*) from visita where número_certidão = %s", arg[0]);

  if (mysql_query(m, q)!=0)
    end_with_error(m);

  MYSQL_RES * res = mysql_store_result(m);
  if (res==NULL)
    end_with_error(m);

  MYSQL_ROW row1 = mysql_fetch_row(res);

  free(q);

  asprintf(&q, "select count(*) from contrato where número_certidão = %s",arg[0]);

  if (mysql_query(m, q)!=0)
    end_with_error(m);

  res = mysql_store_result(m);
  if (res==NULL)
    end_with_error(m);

  MYSQL_ROW row2 = mysql_fetch_row(res);

  free(q);

  printf("Esse imóvel teve até agora %s visita(s) e esteve envolvido em %s contrato(s)\n",row1[0],row2[0]);
}
