#ifndef INSERT_H
#define INSERT_H
#include <mysql/mysql.h>

/* Adiciona uma tupla a tabela pessoa e retorna o cpf.
   Aquele que chama essa função é que tem que passar o cpf para o free(). */
extern char * helper_person(MYSQL * m);

/* Todas essas funções são ajudantes para a criação de uma 
   nova tupla nas tabelas apropriadas.
*/
extern void helper_owner(MYSQL * m);
extern void helper_realtors(MYSQL * m);
extern void helper_guarantor(MYSQL * m);
extern void helper_occupant(MYSQL *m);
extern void helper_property(MYSQL * m);
extern void helper_proposal(MYSQL *m);
extern void help_contract(MYSQL *m);
extern void helper_visit(MYSQL * m);

#endif /* INSERT_H */
