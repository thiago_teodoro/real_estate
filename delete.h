#ifndef DELETE_H
#define DELETE_H

#include <mysql/mysql.h>
/* Deletamos a tupla referenciada pelos atributos chaves 
   guardados em arg[1...] da tabela correspondente à operação
   arg[0].
*/
extern void delete(MYSQL * m,const char * arg[]);


#endif /* DELETE_H */
