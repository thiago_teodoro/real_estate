#ifndef UPDATE_H
#define UPDATE_H
#include <mysql/mysql.h>
/* Um ajudante para atualizar alguma tabela referente a operação
 type e cuja tupla deva ter a chave primaria  key.*/
extern void update(MYSQL *m,const char * type,const char * key);

#endif /* UPDATE_H */
