#define _GNU_SOURCE
#include <stdio.h>
#include <err.h>
#include <stdlib.h>

#include <mysql/mysql.h>
#include <readline/readline.h>
#include <readline/history.h>

#include <sys/types.h>
#include <regex.h>

#include <stdbool.h>
#include "query.h"
#include "insert.h"
#include "update.h"
#include "delete.h"

enum
  {
   SHOW_PROPERTY_PATTERN = 0,
   SHOW_OFFERS_PATTERN,
   SHOW_CLIENTS_PROPOSAL_PATTERN,
   SHOW_REALTORS_INCOME_PATTERN,
   SHOW_PROPERTIES_FREQ,
   SHOW_PATTERN,
   ADD_OWNER_PATTERN,
   ADD_GUARANTOR_PATTERN,
   ADD_REALTOR_PATTERN,
   ADD_OCCUPANT_PATTERN,
   ADD_PROPERTY_PATTERN,
   ADD_PROPOSAL_PATTERN,
   ADD_CONTRACT_PATTERN,
   ADD_VISIT_PATTERN,
   UPDATE_PATTERN,
   DELETE_PATTERN,
   N_PATTERN
  };

/* O número máximo de argumentos que uma expressão regular pode conter */
#define MAX_PATTERN_ARGS 6

/* Essas sãos as expressões regulares usadas. */
const char * preg_pattern[] =
  {"^show[ ]+property[ ]+([0-9]+)[ ]*$",
   "^show[ ]+offers[ ]+([0-9]+)[ ]*$",
   "^show[ ]+clients[ ]+proposal[ ]*$",
   "^show[ ]+realtors[ ]+income[ ]*([0-9]+)[ ]*$",
   "^show[ ]+properties-freq[ ]+([0-9]+)[ ]*$",
   "^show[ ]+([a-z]+)[ ]*([0-9]*)[ ]*$", 	/* Esse é o comando show genérico.
Serve para listar o conteúdo das tabelas. */
   "^add[ ]*owner[ ]*$",
   "^add[ ]*guarantor[ ]*$",
   "^add[ ]*realtor[ ]*$",
   "^add[ ]*occupant[ ]*$",
   "^add[ ]*property[ ]*$",
   "^add[ ]*proposal[ ]*$",
   "^add[ ]*contract[ ]*$",
   "^add[ ]*visit[ ]*$",
   "^update[ ]+([a-z]+)[ ]+([^ ]+)[ ]*$",
   "^delete[ ]+([a-z]+)[ ]+([^ ]+)[ ]*([^ ]*)[ ]*([^ ]*)[ ]*([^ ]*)[ ]*([^ ]*)[ ]*$"
  };


#define VOCABULARY_SIZE ((int)(sizeof(vocabulary)/sizeof(char*)))
/* Esse é o vocabulario utilizado para podermos completar com TAB. */
char * vocabulary[] = {"income","proposal",
				      "realtors","clients","offers"
				      ,"property","properties","clients",
				      "show","add","occupant","owner",
				      "guarantor","contract","visit",
				      "update","realtor","delete","occupants",
		       "owners","guarantors","phones","proposals",
		       "contracts","visits","people","person","properties-freq"};



/* Quando TAB é preciosado, os candidatos são gerados por essa função,
   baseados no valor de "text". Para mais detalhes consulte o manual do 
   readline.
*/
char* completion_generator(const char* text, int state) 
  {
    static char * buff[VOCABULARY_SIZE];
    static int index;

    if (state == 0)
      {
	index = 0;
	for (int i = 0; i < VOCABULARY_SIZE; i++)
	  if (strlen(text) <= strlen(vocabulary[i]))
	    if (strncasecmp(text, vocabulary[i], strlen(text)) == 0)
	      buff[index++] = vocabulary[i];
      }

    if (index == 0)
      return NULL;

    return strdup(buff[--index]);
  }

/* Essa é a função que passaremos para a biblioteca do readline que cuida com a
   parte de autocompletar por TAB. Maiores detalhes estão no manual do readline.
 */
char** completer(const char* text, int start, int end)
{
  // Desabilita nomes de arquivos como candidatos.
  rl_attempted_completion_over = 1;
  return rl_completion_matches(text, completion_generator);
}

int main()
{
  /* Passamos a nossa função para o sistema interno do readline. */
  rl_attempted_completion_function = &completer;
  
  MYSQL * mysql = mysql_init(NULL);
  if (mysql == NULL)
    end_with_error(mysql);

  if (mysql_real_connect(mysql, NULL, "client", "client", "imobiliária", 0, 0, 0) == NULL)
    end_with_error(mysql);


  /* Esse trecho é responsável por compilar as expressões regulares.
     Guardando o resultado em preg.
   */
  regex_t preg[N_PATTERN];

  for (int i = 0; i < N_PATTERN; i++)
    if (regcomp(&preg[i], preg_pattern[i],
		REG_ICASE|REG_EXTENDED)!=0)
      err(-1,"Não foi possível compilar a expressão regular.\n");

  
  /* Esse é o loop básico do nosso cliente. */
  char * line;
  while ((line = readline("->")))
    {
      /* Adicionamos a entrada ao histórico. */
      add_history(line);
      int pattern_type = -1;

      /* Aqui ficará guardado todos os possíveis matchs que podem
	 ocorrer na expressão regular.
      */
      regmatch_t pmatch[(MAX_PATTERN_ARGS+1)];

      bool comando_aceito = false;
      /* Checamos se a entrada satisfaz alguma expressão definida anteriormente. */
      for (int i = 0; i < N_PATTERN; i++)
	{
	  if (regexec(&preg[i], line, (MAX_PATTERN_ARGS+1),
		      pmatch, 0) == 0)
	    {
	      pattern_type = i;
	      comando_aceito = true;
	      break;
	    }
	}
      
      if (!comando_aceito)
	{
	  printf("Command not found: %s\n",line);
	  continue;
	}

      /* Guardamos todos os argumentos encontrados. */
      char * arg[MAX_PATTERN_ARGS];
      for (int i = 0; i < MAX_PATTERN_ARGS; i++)
	{
	  arg[i] = NULL;
	  if (pmatch[i+1].rm_eo != -1 &&
	      pmatch[i+1].rm_so != -1)
	    {
	      *(line + pmatch[i+1].rm_eo) = 0;
	      arg[i] = line + pmatch[i+1].rm_so;
	    }
	}

      /* De acordo com o tipo da consulta realizamos a operação necessária. */
      switch (pattern_type)
	{
	case SHOW_PATTERN:
	  standard_query(mysql, arg);
	  break;
	case SHOW_CLIENTS_PROPOSAL_PATTERN:
	  client_proposal_query(mysql);
	  break;
	case SHOW_OFFERS_PATTERN:
	  offers_query(mysql,arg[0]);
	  break;
	case SHOW_PROPERTY_PATTERN:
	  property_query(mysql,arg[0]);
	  break;
	case SHOW_REALTORS_INCOME_PATTERN:
	  income_query(mysql, arg[0]);
	  break;
	case ADD_OWNER_PATTERN:
	  helper_owner(mysql);
	  break;
	case ADD_GUARANTOR_PATTERN:
	  helper_guarantor(mysql);
	  break;
	case ADD_REALTOR_PATTERN:
	  helper_realtors(mysql);
	  break;
	case ADD_OCCUPANT_PATTERN:
	  helper_occupant(mysql);
	  break;
	case ADD_PROPERTY_PATTERN:
	  helper_property(mysql);
	  break;
	case ADD_PROPOSAL_PATTERN:
	  helper_proposal(mysql);
	  break;
	case ADD_CONTRACT_PATTERN:
	  help_contract(mysql);
	  break;
	case ADD_VISIT_PATTERN:
	  helper_visit(mysql);
	  break;
	case UPDATE_PATTERN:
	  update(mysql, arg[0], arg[1]);
	  break;
	case DELETE_PATTERN:
	  delete(mysql, arg);
	  break;
	case SHOW_PROPERTIES_FREQ:
	  properties_freq_query(mysql, arg);
	  break;
	}
      

      free(line);
    }

  mysql_close(mysql);
  return 0;
}
