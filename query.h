#ifndef QUERY_H
#define QUERY_H

#include <mysql/mysql.h>

/* Esse método é responsável pela consulta básica 
   a uma tabela. Ou seja, apenas lista as tuplas dela,
   sem fazer filtragem sobre os seus atributos.
   O vetor arg deve conter o tipo de operação(client,owner,etc)
   e o número de linhas a serem imprimidas, caso houver.
 */
extern void
standard_query(MYSQL * mysql,const char * arg[]);

/* Exibe todas as informações referentes ao imóvel referenciado
pelo número de certidão "number".*/
extern void
property_query(MYSQL * mysql,char * number);

/* Exibe todas as ofertas recebidas pelo imóvel "number". */
extern void
offers_query(MYSQL * mysql,char * number);
/* Exibe o corretor com o maior rendimento no ano "year". */
extern void
income_query(MYSQL * mysql,const char * year);
/* Exibe todos os clientes que fizeram alguma proposta. */
extern void
client_proposal_query(MYSQL *m);

/* m e arg são como no primeiro caso */
extern void
properties_freq_query(MYSQL * m,char * arg[]);

/* Imprime o erro que ocorreu e encerra o programa. */
extern void end_with_error(MYSQL * m);


#endif /* QUERY_H */
