#define _GNU_SOURCE
#include <stdio.h>
#include <err.h>
#include <stdlib.h>

#include "query.h"
#include "insert.h"

#include <readline/readline.h>
#include <readline/history.h>

/* 
   Pequeno método para deletar uma tupla da tabela
   pessoa.
*/
void delete_person(MYSQL * m,const char * cpf)
{
  char * q;
  asprintf(&q,"delete from pessoa where cpf = %s",cpf);
  if (mysql_query(m,q) != 0)
    end_with_error(m);

  free(q);
}

/* Adiciona uma tupla a tabela pessoa e retorna o cpf.
   Aquele que chama essa função é que tem que passar o cpf para o free(). */
char * helper_person(MYSQL * m)
{

  /* Inserimos os dados referentes a tabela pessoa */
  char * cpf = readline("CPF:");
  char * nome = readline("Nome:");

  char * q;
  asprintf(&q, "insert into pessoa (cpf,nome) select * from "
	   "(select %s as f,\"%s\" as s) as tmp where "
	   "not exists (select * from pessoa "
	   "where cpf = tmp.f and nome = tmp.s)", cpf,nome);
  
  if (mysql_query(m, q) != 0)
    end_with_error(m);
  
  free(q);

  /* Inserimos o número de telefone */
  puts("Pressione Ctrl-D para parar de adicionar telefones");
  char * num;
  while ((num = readline("Número:")))
    {
      asprintf(&q, "insert into telefone values (%s,%s)", num,cpf);

      /* Tentamsos inserir o número. Em caso de erro, removemos
	 a tupla inserida da tabela pessoa.
      */
      if (mysql_query(m, q) != 0)
	{
	  warn("%s\n",mysql_error(m));
	  delete_person(m, cpf);
	  exit(-1);
	}

      free(q);
      free(num);
    }
  puts("");
  
  free(nome);
  return cpf;
}

/* Inserimos os dados relativos à tabela proprietário. */
void helper_owner(MYSQL * m)
{
  char * cpf = helper_person(m);
  char * estado = readline("Estado civil:");
  char * q;
  asprintf(&q, "insert into proprietário values (\"%s\",%s)",
	   estado,cpf);

  /* Tentamos inserir o estado civil. Em caso de erro, removemos
     a tupla inserida da tabela pessoa. Note que isso irá remover
     também o telefone.
  */
  if (mysql_query(m, q) != 0)
    {
      warn("%s\n",mysql_error(m));
      delete_person(m, cpf);
      exit(-1);
    }


  free(cpf);
  free(estado);
  free(q);
}

/* Inserimos os dados relativos à tabela corretor. */
void helper_realtors(MYSQL * m)
{
  char * cpf = helper_person(m);
  char * creci = readline("CRECI:");
  char * data_inicio = readline("Data de admissão:");
  char * comissao  = readline("Valor da comissão:");
  char *q;

  asprintf(&q, "insert into corretor values(%s,%s,\'%s\',%s)",cpf,
	   creci,data_inicio,comissao);
  
  if (mysql_query(m,q) != 0)
    {
      warnx("%s\n",mysql_error(m));
      delete_person(m, cpf);
      exit(-1);
    }
  
  free(comissao);
  free(data_inicio);
  free(creci);
  free(cpf);
}

/* Inserimos os dados da tabela fiador. */
void helper_guarantor(MYSQL * m)
{
  char * cpf = helper_person(m);
  char * saldo,*q;

  saldo = readline("Saldo bancário:");
  asprintf(&q, "insert into fiador values(%s,%s)",saldo,cpf);
  if (mysql_query(m,q) != 0)
    {
      warn("%s\n",mysql_error(m));
      delete_person(m, cpf);
      exit(-1);
    }

  free(cpf);
  free(saldo);
  free(q);
}
/* Inserimos os dados do inquilino. */
void helper_occupant(MYSQL *m)
{
  char * cpf = helper_person(m);
  char * prof = readline("Profissão:");
  char * renda = readline("Renda familiar:");
  char * cpf1 = readline("CPF Fiador(1):");
  char * cpf2 = readline("CPF Fiador(2):");
  char * q;

  asprintf(&q, "insert into inquilino values(\"%s\",%s,%s,%s,%s)",
	   prof,renda,cpf,cpf1,cpf2);

  if (mysql_query(m,q) != 0)
    {
      warn("%s\n",mysql_error(m));
      delete_person(m, cpf);
      exit(-1);
    }

  free(cpf1);
  free(cpf2);
  free(renda);
  free(prof);
  free(cpf);
}
/* Inserimos um imóvel. */
void helper_property(MYSQL * m)
{
  char * nc = readline("Número de certidão:");
  char * d = readline("Data registro:");
  char * cid = readline("Cidade:");
  char * bai = readline("Bairro:");
  char * rua = readline("Rua:");
  char * comp = readline("Complemento:");
  char * cep = readline("CEP:");
  char * n = readline("Número:");
  char * co = readline("Cômodos:");
  char * vag = readline("Vagas na garagem:");
  char * a = readline("Área:");
  char * alu = readline("Aluguel:");
  char * dat = readline("Data:");
  char * cpf = readline("CPF proprietário:");
  char * q;

  asprintf(&q, "insert into imóveis values(%s,\'%s\',\"%s\",\"%s\",\"%s\",\"%s\",%s,%s,%s,%s,%s,%s,\'%s\',%s)",
	   nc,d,cid,bai,rua,comp,cep,n,co,vag,a,alu,dat,cpf);

  if (mysql_query(m,q) != 0)
    end_with_error(m);
  
  free(q);
  free(nc);
  free(d);
  free(cid);
  free(bai);
  free(rua);
  free(comp);
  free(cep);
  free(n);
  free(co);
  free(vag);
  free(a);
  free(alu);
  free(dat);
  free(cpf);
}
/* Adicionamos uma proposta. */
void helper_proposal(MYSQL *m)
{
  char * cpf = readline("CPF Inquilino:");
  char * num = readline("Número certidão:");
  char * data_p = readline("Data da proposta:");
  char * val = readline("Valor:");
  char * data_v = readline("Data de validade:");
  char * q;

  asprintf(&q, "insert into proposta values(%s,%s,\'%s\',%s,\'%s\')",
	   cpf,num,data_p,val,data_v);

  if (mysql_query(m,q) != 0)
    end_with_error(m);

  free(q);
  free(cpf);
  free(num);
  free(data_p);
  free(val);
  free(data_v);
}

/* Adicionamos um contrato. */
void help_contract(MYSQL *m)
{

  char * cpf = readline("CPF do inquilino:");
  char * nc = readline("Número da certidão:");
  char * di = readline("Data de início:");
  char * dt = readline("Data de término:");
  char * v = readline("Valor:");
  char *q;

  asprintf(&q, "insert into contrato(número_certidão,data_início,data_término,valor,cpf_inquilino) values(%s,\'%s\',\'%s\',%s,%s)",
	   nc,di,dt,v,cpf);
  if (mysql_query(m,q) != 0)
    end_with_error(m);
  
  free(nc);
  free(di);
  free(dt);
  free(v);
  free(q);
}

/* Adicionamos uma visita. */
void helper_visit(MYSQL * m)
{
  char * cpfc = readline("CPF Corretor:");
  char * num = readline("Número certidão:");
  char * cpfi = readline("CPF Inquilino:");
  char * d = readline("Data:");
  char * h = readline("Horário:");
  char * q;

  asprintf(&q, "insert into visita values(%s,%s,%s,\'%s\',\'%s\')"
	   ,cpfc,num,cpfi,d,h);

  if (mysql_query(m,q) != 0)
    end_with_error(m);
  
  free(cpfc);
  free(num);
  free(cpfi);
  free(d);
  free(h); 
}
