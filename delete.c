#define _GNU_SOURCE
#include <stdio.h>
#include <err.h>
#include <stdlib.h>
#include <readline/readline.h>
#include <readline/history.h>
#include "query.h"
#include "delete.h"

/* Enumeração para referenciar a matriz abaixo. */
enum
  {
   OCCUPANT = 0,
   OWNER,
   GUARANTOR,
   REALTOR,
   PROPERTY,
   PHONE,
   VISIT,
   PROPOSAL,
   CONTRACT,
   PERSON,
   N_MAP
  };

/* Fazemos o mapeamento das operações nas suas respectivas tabelas.*/
const char * map[N_MAP][2] =
  {
   {"occupant","inquilino"},
   {"owner","proprietário"},
   {"guarantor","fiador"},
   {"realtor","corretor"},
   {"property","imóveis"},
   {"phone","telefone"},
   {"visit","visita"},
   {"proposal","proposta"},
   {"contract","contrato"},
   {"person","pessoa"}
  };

void delete(MYSQL * m,const char * arg[])
{
  /* Procuramos a tabela adequada. */
  int t = -1;
  for (int i = 0; i < N_MAP; i++)
    if (strcmp(arg[0], map[i][0])==0)
      {
	t = i;
	break;
      }

  /* Se foi encontrada a tabela correspondente, geramos 
     a condição que será passada para a cláusula where e 
     montamos a consulta final.
  */
  if (t != -1)
    {
      char * cond;
      switch (t)
	{
	case OCCUPANT:
	case OWNER:
	case GUARANTOR:
	case REALTOR:
	  asprintf(&cond, "cpf = %s",arg[1]);
	  break;
	case PROPERTY:
	  asprintf(&cond, "número_certidão = %s",arg[1]);
	  break;
	case PHONE:
	  asprintf(&cond, "número = %s",arg[1]);
	  break;
	case VISIT:
	  asprintf(&cond, "cpf_corretor = %s and número_certidão = %s and cpf_inquilino = %s and data = \'%s\' and hora = \'%s\'",
		   arg[1],arg[2],arg[3],arg[4],arg[5]);
	  break;
	case PROPOSAL:
	  asprintf(&cond, "cpf_inquilino = %s and número_certidão = %s",arg[1],arg[2]);
	  break;
	case CONTRACT:
	  asprintf(&cond, "número_contrato = %s",arg[1]);
	  break;
	case PERSON:
	  asprintf(&cond, "cpf = %s",arg[1]);
	  break;
	}

      char * qstr;
      asprintf(&qstr, "delete from %s where %s",map[t][1],cond);

      if (mysql_query(m, qstr)!=0)
	end_with_error(m);
      
      free(qstr);
      free(cond);
    }
  else
    printf("Comando \"%s\" não reconhecido.\n",arg[0]);
}
